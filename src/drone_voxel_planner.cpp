#include "drone_voxel_planner/drone_voxel_planner.h"

/* ------------------------------------------------------------------ */

VoxelPlanner::VoxelPlanner() {
  // get topic name parameter
  if (!nh_.getParam("/drone_voxel_planner_node/voxel_goal_topic",goal_topic_)) {
    goal_topic_ = "/way_point";
  }
  if (!nh_.getParam("/drone_voxel_planner_node/voxel_score_topic", voxel_score_topic_)) {
    voxel_score_topic_ = "/score_matrix";
  }
  if (!nh_.getParam("/drone_voxel_planner_node/voxel_odom_topic",odom_topic_)) {
    odom_topic_ = "/integrated_to_map";
  }
  if (!nh_.getParam("/drone_voxel_planner_node/uav_goal_frame",robot_frame_id_)) {
    robot_frame_id_ = "world";
  }

  if (!nh_.getParam("/drone_voxel_planner_node/planner_command_topic",planner_command_topic_)) {
    planner_command_topic_ = "drone_voxel_planner_command";
  }

  if (!nh_.getParam("/drone_voxel_planner_node/planner_status_topic",planner_status_topic_)) {
    planner_status_topic_ = "drone_voxel_planner_status";
  }

  if (!nh_.getParam("/drone_voxel_planner_node/use_command_to_run", use_command_to_run_)) {
    use_command_to_run_ = false;
  }

  if (!nh_.getParam("/drone_voxel_planner_node/main_loop_frequency", main_loop_frequency_)) {
    main_loop_frequency_ = 10.0f;
  }


  // initial cloud
  voxel_score_ = pcl::PointCloud<pcl::PointXYZI>::Ptr(new pcl::PointCloud<pcl::PointXYZI>());
  // initial old principal direciton
  old_open_direction_3D_ = this->CPoint3D(0,0,0);
  std::cout<<"Initialize Successfully"<<std::endl;
}

Point3D VoxelPlanner::CPoint3D(float x, float y, float z) {
  Point3D p;
  p.x = x;
  p.y = y;
  p.z = z;
  return p;
}

void VoxelPlanner::PlannerCommandHandler(const drone_voxel_planner::DroneVoxelPlannerCommandConstPtr&
  command_msg) {

  run_planner_ = command_msg->command;
}

void VoxelPlanner::Loop3D() {
  // Loop Subscriber
  rviz_direct_pub_ = nh_.advertise<nav_msgs::Path>("/vb_planner/PCA_direction",1);
  goal_pub_ = nh_.advertise<geometry_msgs::PoseStamped>(goal_topic_,1);
  voxel_score_sub_ = nh_.subscribe(voxel_score_topic_,1, &VoxelPlanner::VoxelScoreHandler, this);
  odom_sub_ = nh_.subscribe(odom_topic_,1,&VoxelPlanner::OdomHandler,this);
  planner_command_sub_ = nh_.subscribe(planner_command_topic_, 1,
    &VoxelPlanner::PlannerCommandHandler,
  this);
  planner_status_pub_ = nh_.advertise<drone_voxel_planner::DroneVoxelPlannerStatus>
  (planner_status_topic_, 1);

  ros::Rate rate(main_loop_frequency_);
  while(ros::ok())
  {
    ros::spinOnce(); // process all callback function

    if (use_command_to_run_ && run_planner_) {
      if (!voxel_score_->empty()) {
        old_open_direction_3D_ = this->OpenDirectionAnalysis3D();
        this->HandleWaypoint3D(); // Log frame id, etc. -> goal
        progress_status_ = true;
        VoxelPlanner::PublishPlannerStatus(progress_status_,
          local_frontiers_present_);
      }
      rate.sleep();
    }
    else if(!use_command_to_run_) {
      if (!voxel_score_->empty()) {
        old_open_direction_3D_ = this->OpenDirectionAnalysis3D();
        this->HandleWaypoint3D(); // Log frame id, etc. -> goal
        progress_status_ = true;
        VoxelPlanner::PublishPlannerStatus(progress_status_,
          local_frontiers_present_);
      }
      rate.sleep();
    }
    else {
      VoxelPlanner::PublishPlannerStatus(false,
        false);
    }
  }
}

void VoxelPlanner::OdomHandler(const nav_msgs::Odometry odom_msg) {
  // Credit: CMU SUB_T dfs_behavior_planner, Chao C.,
  // https://bitbucket.org/cmusubt/dfs_behavior_planner/src/master/src/dfs_behavior_planner/dfs_behavior_planner.cpp
  odom_ = odom_msg;
  robot_pos_.x = odom_.pose.pose.position.x;
  robot_pos_.y = odom_.pose.pose.position.y;
  robot_pos_.z = odom_.pose.pose.position.z;

  double roll, pitch, yaw;
  geometry_msgs::Quaternion geo_quat = odom_msg.pose.pose.orientation;
  tf::Matrix3x3(tf::Quaternion(geo_quat.x, geo_quat.y, geo_quat.z, geo_quat.w)).getRPY(roll, pitch, yaw);
  // std::cout<<"Debug Yaw: "<< yaw << std::endl;
  robot_heading_.x = cos(yaw);
  robot_heading_.y = sin(yaw);
  robot_heading_.z = 0;
  // std::cout<<"Heading X: "<<  robot_heading_.x << "Heading Y: "<<  robot_heading_.y << std::endl;
  if (old_open_direction_3D_ == this->CPoint3D(0,0,0)) {
    old_open_direction_3D_ = robot_heading_;

    // DEBUG
    // std::cout << "Odom has been updated >>>>>" <<std::endl;
  }
}

Point3D VoxelPlanner::OpenDirectionAnalysis3D() {
  this->VoxelScoreAssign(direct_score_stack_);
  int num_direct = direct_stack_3D_.size();
  Point3D open_direct, view_point;
  open_direct = direct_stack_3D_[int(num_direct/2)];
  view_point = waypoint_stack_3D_[int(num_direct/2)];
  float high_score = -std::numeric_limits<float>::infinity();;
  for (int i=0; i<num_direct; i++) {
    float direct_scale;
    if (old_open_direction_3D_ == this->CPoint3D(0,0,0)) {
      direct_scale = 1;
    }else direct_scale = direct_stack_3D_[i] * old_open_direction_3D_;

    // DEBUG
    // std::cout<<"Direction_scale: " << direct_scale << std::endl;

    float score = direct_score_stack_[i] * direct_scale;
    if (score > high_score && (old_open_direction_3D_ == this->CPoint3D(0,0,0) || old_open_direction_3D_ * direct_stack_3D_[i] > 0 )) {
      high_score = score;
      open_direct = direct_stack_3D_[i];
      view_point = waypoint_stack_3D_[i];
    }
  }
  // DEBUG
  // std::cout<<"High Score: " << high_score << std::endl;

  if (high_score != 0) {
    open_direction_3D_ = open_direct;
    open_view_point_ = view_point;
    local_frontiers_present_ = true;
  }else {
    // DEBUG
    ROS_WARN_THROTTLE(2, "!! No local frontiers found");
    local_frontiers_present_ = false;
    open_direction_3D_ = open_direction_3D_;
    open_view_point_ = open_view_point_;
  }
  return open_direction_3D_;
}


void VoxelPlanner::VoxelScoreAssign(std::vector<float>& score_array) {
  /* Assign value to diect_score_stack_ & diect_stack from voxel_score_ info */
  score_array.clear();
  direct_stack_3D_.clear();
  waypoint_stack_3D_.clear();
  std::size_t num_voxels = voxel_score_->points.size();
  for (std::size_t i=0; i<num_voxels; i++) {
    Point3D waypoint_3d, direct_3d;
    waypoint_3d.x = voxel_score_->points[i].x;
    waypoint_3d.y = voxel_score_->points[i].y;
    waypoint_3d.z = voxel_score_->points[i].z;

    // DEBUG
    // std::cout << "X: " << waypoint_3d.x << std::endl;

    direct_3d.x = voxel_score_->points[i].x - robot_pos_.x;
    direct_3d.y = voxel_score_->points[i].y - robot_pos_.y;
    direct_3d.z = voxel_score_->points[i].z - robot_pos_.z;
    this->Normalize(direct_3d);
    waypoint_stack_3D_.push_back(waypoint_3d);
    direct_stack_3D_.push_back(direct_3d);
    score_array.push_back(voxel_score_->points[i].intensity);
    if (score_array[-1] != 0) {
      std::cout<<"Non zero score: "<<score_array[-1]<<std::endl;
    }
  }
}

void VoxelPlanner::HandleWaypoint3D() {
  Point3D angle;
  tf2::Quaternion myQuaternion;
  geometry_msgs::Quaternion q;
  goal_waypoint_.header = cloud_header_;
  goal_waypoint_.header.frame_id = robot_frame_id_;
  goal_waypoint_.pose.position.x = open_view_point_.x;
  goal_waypoint_.pose.position.y = open_view_point_.y;
  goal_waypoint_.pose.position.z = open_view_point_.z;
  this->AngleConvert(old_open_direction_3D_, angle);
  myQuaternion.setRPY(angle.x, angle.y, angle.z);
  tf2::convert(myQuaternion,q);
  goal_waypoint_.pose.orientation = q;
  goal_pub_.publish(goal_waypoint_);
}


void VoxelPlanner::VoxelScoreHandler(const sensor_msgs::PointCloud2ConstPtr laser_msg) {
  /* CallBack function for /score_matrix topic*/
  voxel_score_->clear();
  cloud_header_ = laser_msg->header;
  pcl::fromROSMsg(*laser_msg, *voxel_score_);
}

float VoxelPlanner::Norm3D(const Point3D& p) {
  return sqrt(p.x*p.x + p.y*p.y + p.z*p.z);
}

void VoxelPlanner::Normalize(Point3D& p) {
  float norm = this->Norm3D(p);
  p.x = p.x/norm;
  p.y = p.y/norm;
  p.z = p.z/norm;
  return;
}

void VoxelPlanner::AngleConvert(const Point3D& vec, Point3D& angle) {
  // Yaw is the bearing of the forward vector's shadow in the xy plane.
  float yaw = atan2(vec.y, vec.x);

  // Pitch is the altitude of the forward vector off the xy plane, toward the down direction.
  float pitch = asin(-vec.z/sqrt(vec.x*vec.x + vec.y*vec.y));

  // Convert radians to degrees.
  angle.z = yaw;
  angle.y = pitch;
  angle.x = 0;
}

void VoxelPlanner::PublishPlannerStatus(bool progress_status_,
                                bool local_frontiers_present_) {

  drone_voxel_planner::DroneVoxelPlannerStatusPtr planner_status_msg (new
                            drone_voxel_planner::DroneVoxelPlannerStatus);
  planner_status_msg->header.stamp = ros::Time::now();
  if (progress_status_) {
    planner_status_msg->status = drone_voxel_planner::DroneVoxelPlannerStatus
    ::STATUS_IN_PROGRESS;
  } else {
    planner_status_msg->status = drone_voxel_planner::DroneVoxelPlannerStatus
    ::STATUS_DISABLED;
  }
  planner_status_msg->has_local_frontiers = local_frontiers_present_;
  planner_status_pub_.publish(planner_status_msg);

}

          /* ---------------------------------------------------------------------------- */
