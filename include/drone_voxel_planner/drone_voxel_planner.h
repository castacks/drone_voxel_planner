#ifndef DRONE_VOXEL_PLANNER_H
#define DRONE_VOXEL_PLANNER_H


#include <ros/ros.h>
#include <std_msgs/Header.h>
#include <nav_msgs/Path.h>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl/point_cloud.h>
#include <pcl_conversions/pcl_conversions.h>
#include <tf/transform_datatypes.h>
#include <geometry_msgs/PointStamped.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Quaternion.h>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <limits>



#include <vector>
#include <string>
#include <math.h>

#include <drone_voxel_planner/DroneVoxelPlannerCommand.h>
#include <drone_voxel_planner/DroneVoxelPlannerStatus.h>

typedef geometry_msgs::Point ROSPoint;
typedef geometry_msgs::PoseStamped ROSPose;

struct Point3D {
    float x;
    float y;
    float z;
    Point3D() {}
    Point3D(float _x, float _y, float _z): x(_x), y(_y), z(_z) {}
    bool operator ==(const Point3D& pt) const
    {
        return x == pt.x && y == pt.y && z == pt.z;
    }

    float operator *(const Point3D& pt) const
    {
        return x * pt.x + y * pt.y + z * pt.z;
    }

    Point3D operator *(const float factor) const
    {
        return Point3D(x*factor, y*factor, z*factor);
    }

    Point3D operator /(const float factor) const
    {
        return Point3D(x/factor, y/factor, z/factor);
    }

    Point3D operator +(const Point3D& pt) const
    {
        return Point3D(x+pt.x, y+pt.y, z+pt.z);
    }
    Point3D operator -(const Point3D& pt) const
    {
        return Point3D(x-pt.x, y-pt.y, z-pt.z);
    }
};


class VoxelPlanner
{
public:
    VoxelPlanner();
    void Loop3D();

private:
    // rostopic define
    ros::NodeHandle nh_;
    ros::Subscriber voxel_score_sub_;
    ros::Subscriber odom_sub_;
    ros::Subscriber planner_command_sub_;
    ros::Publisher planner_status_pub_;
    ros::Publisher goal_pub_;
    ros::Publisher rviz_direct_pub_;
    // function define
    Point3D CPoint3D(float x, float y, float z);
    float Norm3D(const Point3D& p);
    // Point PrincipalAnalysis();
    Point3D OpenDirectionAnalysis3D();
    void HandleWaypoint3D(); // publish goal and rviz
    void VoxelScoreAssign(std::vector<float>& score_array);
    void VoxelScoreHandler(const sensor_msgs::PointCloud2ConstPtr laser_msg);
    void OdomHandler(const nav_msgs::Odometry odom_msg);
    void Normalize(Point3D& p);
    void AngleConvert(const Point3D& vec, Point3D& angle);
    void PlannerCommandHandler(const drone_voxel_planner::DroneVoxelPlannerCommandConstPtr&
                                command_msg);
    void PublishPlannerStatus(bool progress_status,
                              bool local_frontiers_present_);

    // valuable define
    std_msgs::Header cloud_header_;
    std::vector<Point3D> direct_stack_3D_;
    std::vector<Point3D> waypoint_stack_3D_;
    std::vector<float> direct_score_stack_;
    ROSPose goal_waypoint_;
    // Point principal_direction_;
    int center_index_;
    Point3D robot_pos_;
    Point3D robot_heading_;
    Point3D open_direction_3D_;
    Point3D open_view_point_;
    Point3D old_open_direction_3D_;
    nav_msgs::Odometry odom_;
    nav_msgs::Path rviz_direction_;
    pcl::PointCloud<pcl::PointXYZI>::Ptr voxel_score_;
    // ros  parameter value
    std::string robot_frame_id_;
    std::string goal_topic_, voxel_score_topic_, odom_topic_;
    std::string planner_command_topic_, planner_status_topic_;
    bool progress_status_, local_frontiers_present_;
    bool use_command_to_run_;
    uint run_planner_;
    float main_loop_frequency_;
};

#endif
