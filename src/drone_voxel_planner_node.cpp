#include "drone_voxel_planner/drone_voxel_planner.h"


/* ---------------------------------------------------------------------------- */

int main(int argc, char** argv) {
    ros::init(argc, argv, "vb_planner");
    VoxelPlanner voxel_planner;
    voxel_planner.Loop3D();
    return 0;
}